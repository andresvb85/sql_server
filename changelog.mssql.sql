--liquibase formatted sql

--changeset bob:1
create table test1(
id int primary key,
name varchar(255)
);

--changeset bob:2
create table test2(
id int primary key,
name varchar(255)
);

--changeset bob:3
create table test3(
id int primary key,
name varchar(255)
);

--changeset bob:4
INSERT INTO [dbo].[test1] VALUES(5,'Prueba')

--changeset bob:5
TRUNCATE TABLE [dbo].[test1]

--changeset bob:6
INSERT INTO [dbo].[test1] VALUES(6,'PruebaGitLab')

--changeset bob:7
INSERT INTO [dbo].[test1] VALUES(7,'PruebaGitLab')

--changeset bob:8
INSERT INTO [dbo].[test1] VALUES(8,'PruebaGitLab')

--changeset bob:9
create table test4(
id int primary key,
name varchar(255)
);
